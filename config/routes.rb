Rails.application.routes.draw do
  namespace :api, default: {format: :json} do
    namespace :v1 do
      resources :users, :except => [:new, :edit]
      resources :categories, :except => [:new, :edit, :show]
      resources :quizzes, :except => [:new, :edit, :update]
      resources :questions, :except => [:new, :edit, :show, :update]
      resources :answers, :except => [:new, :edit, :show]
      resources :assigned_quizzes, :except => [:new, :edit, :destroy]
      resources :results, :except => [:new, :edit]
      resources :saved_quizzes, :only => [:index, :create]
      resources :saved_answers, :only => [:index, :create]
      resources :access, :only => [:create, :destroy]
    end
  end
end
