class CreateSavedAnswers < ActiveRecord::Migration
  def change
    create_table :saved_answers do |t|
      t.integer :question_id
      t.integer :result_id
      t.text :users_answer
      t.timestamps null: false
    end
  end
end
