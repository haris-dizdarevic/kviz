class CreateAssignedQuizzes < ActiveRecord::Migration
  def change
    create_table :assigned_quizzes do |t|
      t.integer :user_id
      t.integer :quiz_id
      t.boolean :active

      t.timestamps null: false
    end
  end
end
