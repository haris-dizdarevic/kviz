class CreateResults < ActiveRecord::Migration
  def change
    create_table :results do |t|
      t.float :score
      t.integer :quiz_id
      t.integer :user_id
    end
  end
end
