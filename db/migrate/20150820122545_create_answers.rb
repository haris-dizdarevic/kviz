class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.text :answer_text
      t.boolean :correct
      t.integer :question_id
    end
  end
end
