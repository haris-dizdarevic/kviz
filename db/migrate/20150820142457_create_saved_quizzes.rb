class CreateSavedQuizzes < ActiveRecord::Migration
  def change
    create_table :saved_quizzes do |t|
      t.integer :result_id
      t.integer :answer_id

      t.timestamps null: false
    end
  end
end
