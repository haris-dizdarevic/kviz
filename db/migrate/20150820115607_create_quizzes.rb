class CreateQuizzes < ActiveRecord::Migration
  def change
    create_table :quizzes do |t|
      t.integer :user_id
      t.integer :category_id
      t.string :quiz_type

      t.timestamps null: false
    end
  end
end
