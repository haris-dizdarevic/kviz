namespace :db do
  desc 'Erase and fill database'
  task :populate => :environment do
    require 'populator'
    require 'faker'

    [User, Category, Quiz, Question, Answer, AssignedQuiz, Result, SavedQuiz].each(&:delete_all)

    User.populate 2 do |user|
      user.username = Faker::Internet.user_name
      user.first_name = Faker::Name.first_name
      user.last_name = Faker::Name.last_name
      user.email = Faker::Internet.email
      user.password_digest = Populator.words(1)
      user.role = ['admin', 'user']
      user.created_at = Date.today
    end

    Category.populate 2 do |category|
      category.category_name = Faker::Lorem.word
    end

    User.all.each do |user|
      Category.all.each do |category|
        Quiz.populate 1 do |quiz|
          quiz.user_id = user.id
          quiz.category_id = category.id
          quiz.quiz_type = ['select', 'input']
          quiz.created_at = Date.today
        end
      end
    end

    Quiz.all.each do |quiz|
      Question.populate 5 do |question|
        question.question_text = Faker::Lorem.sentence(4)
        question.quiz_id = quiz.id
      end
    end

    Question.all.each do |question|
      Answer.populate 3 do |answer|
        answer.answer_text = Faker::Lorem.words(2)
        answer.correct = ['true', 'false']
        answer.question_id = question.id
      end
    end

    User.all.each do |user|
      Quiz.all.each do |quiz|
        AssignedQuiz.populate 1 do |assigned_quiz|
          assigned_quiz.user_id = user.id
          assigned_quiz.quiz_id = quiz.id
          assigned_quiz.active = ['true', 'false']
          assigned_quiz.created_at = Date.today
        end
      end
    end

    User.all.each do |user|
      Quiz.all.each do |quiz|
        Result.populate 1 do |result|
          result.user_id = user.id
          result.quiz_id = quiz.id
          result.score = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
        end
      end
    end

    # Result.all.each do |result|
    #   Question.all.each do |question|
    #     Answer.all.each do |answer|
    #       SavedQuiz.populate 1 do |saved_answer|
    #         saved_answer.users_answer = answer.id
    #         saved_answer.question_id = question.id
    #         saved_answer.result_id = result.id
    #         saved_answer.created_at = Date.today
    #       end
    #     end
    #   end
    # end

    puts 'Finished seeding database'
  end
end