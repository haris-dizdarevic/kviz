class Api::V1::QuestionsController < ApplicationController

  load_and_authorize_resource

  def index
    render json: @questions, status: :ok , :include => {:answers => {:only => [:id, :answer_text, :correct, :question_id]}}
  end

  def create
    if @question.save
      render json: @question, status: :ok
    else
      render json: @question.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @question.destroy
      render nothing: true, status: :ok
    else
      render json: @question.errors, status: :unprocessable_entity
    end
  end

  def question_params
    params.require(:question).permit(:question_text, :quiz_id)
  end

end
