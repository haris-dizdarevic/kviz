class Api::V1::QuizzesController < ApplicationController

  load_and_authorize_resource

  def index
    render json: @quizzes, status: :ok, :include => [:user, :category]
  end

  def show
    render json: @quiz , status: :ok, :include => [:category,:questions => {:include => :answers}]
  end

  def create
    if @quiz.save
      render json: @quiz, status: :ok
    else
      render json: @quiz.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @quiz.destroy
      render nothing: true, status: 200
    else
      render json: @quiz.errors, status: :unprocessable_entity
    end
  end

  def quiz_params
    params.require(:quiz).permit(:user_id, :category_id, :quiz_type)
  end

end
