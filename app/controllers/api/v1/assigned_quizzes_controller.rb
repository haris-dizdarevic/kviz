class Api::V1::AssignedQuizzesController < ApplicationController

  load_and_authorize_resource

  def index
    render json: @assigned_quizzes, status: :ok, :include => {:quiz => {:include => :category}}
  end

  def show
    render json: @assigned_quiz, status: :ok
  end

  def create
    puts params
    @assigned_quiz.user_id = get_user.id
    if @assigned_quiz.save
      render json: @assigned_quiz, status: :ok
    else
      render json: @assigned_quiz.errors, status: :unprocessable_entity
    end
  end

  def update
    if @assigned_quiz.update_attributes(assigned_quiz_params)
      render json: @assigned_quiz, status: :ok
    else
      render json: @assigned_quiz.errors, status: :unprocessable_entity
    end
  end

  private

  def assigned_quiz_params
    params.require(:assigned_quiz).permit(:user_id, :quiz_id, :active, :username)
  end

  def get_user
    User.where(:username => params[:assigned_quiz][:username]).first
  end
end
