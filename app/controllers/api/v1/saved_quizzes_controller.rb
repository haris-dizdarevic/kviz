class Api::V1::SavedQuizzesController < ApplicationController

  load_and_authorize_resource

  def index
    @saved_quizzes= SavedQuiz.where(:result_id => params[:result_id])
    render json: @saved_quizzes, status: :ok
  end

  def create
    if @saved_quiz.save
      render json: @saved_quiz, status: :ok
    else
      render json: @saved_quiz.errors, status: :unprocessable_entity
    end
  end

  private

  def saved_quiz_params
    params.require(:saved_quiz).permit(:result_id, :answer_id, :created_at, :updated_at)
  end

end
