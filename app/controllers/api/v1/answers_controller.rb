class Api::V1::AnswersController < ApplicationController

  load_and_authorize_resource

  def index
    render json: @answers, status: 200
  end

  def create
    if @answer.save
      render json: @answer, status: 200
    else
      render json: @answer.errors, status: :unprocessable_entity
    end
  end

  def update
    if @answer.update_attributes(answer_params)
      render json: @answer, status: :ok
    else
      render json: @answer.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @answer.destroy
      render nothing: true, status: :ok
    else
      render json: @answer.errors, status: :unprocessable_entity
    end
  end

  private
  def answer_params
    params.require(:answer).permit(:answer_text, :correct, :question_id)
  end

end
