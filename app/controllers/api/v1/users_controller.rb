class Api::V1::UsersController < ApplicationController

  load_and_authorize_resource

  def index
    render json: @users
  end

  def show
    render json: @user
  end

  def create

    if already_exists
      render json: {:message => 'User with that username already exists'}, status: 409
    elsif @user.save
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def update
    if @user.update_attributes(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @user.destroy
      render nothing: true
    else
      render json: @user.destroy, status: :unprocessable_entity
    end
  end

  def user_params
    params.require(:user).permit(:username, :first_name, :last_name, :email, :password, :role)
  end

  def already_exists
    User.where(:username => params[:user][:username]).first
  end
end
