class Api::V1::ResultsController < ApplicationController

  load_and_authorize_resource

  def index
    render json: @results, status: :ok, :include => [:user, :quiz => {:include => :category}]
  end

  def show
    render json: @result, status: :ok, :include => {:quiz => {:include => [:category, :questions => {:include => :answers}]}}
  end

  def create
    if @result.save
      render json: @result, status: :ok
    else
      render json: @result.errors, status: :ok
    end
  end

  def update
    if @result.update_attributes(result_params)
      render json: @result, status: 200
    else
      render json: @result.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @result.destroy
      render nothing: true, status: 200
    else
      render @result.errors, status: :unprocessable_entity
    end
  end

  def result_params
    params.require(:result).permit(:score, :quiz_id, :user_id)
  end

end
