class Api::V1::SavedAnswersController < ApplicationController

  load_and_authorize_resource

  def index
    @saved_answers = SavedAnswer.where(:result_id => params[:result_id])
    render json: @saved_answers, status: :ok
  end

  def create
    if @saved_answer.save
      render json: @saved_answer, status: :ok
    else
      render json: @saved_answer.errors, status: :unprocessable_entity
    end
  end

  private

  def saved_answer_params
    params.require(:saved_answer).permit(:result_id, :question_id, :users_answer)
  end
end
