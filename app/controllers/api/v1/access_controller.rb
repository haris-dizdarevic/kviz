class Api::V1::AccessController < ApplicationController

  skip_before_action :restrict_access, :only => :create

  def create
    @user = User.where(:username => params[:access][:username]).first
    if @user && @user.authenticate(params[:access][:password])
      token = generate_new_token
      @user.update_attributes(:auth_token => token)
      render json: @user, :except => :password_digest, status: :ok
    else
      render json: {:message => 'Bad Credentials'}, status: :unauthorized
    end
  end

  def destroy
    user = User.find(params[:id])
    if user && user.update_column(:auth_token,nil)
      render json: {:message => 'Successfully logged out'}, status: :ok
    else
      render json: user.errors, status: :unprocessable_entity
    end
  end

  private

  def access_params
    params.require(:access).permit(:email,:password)
  end

  def generate_new_token
    SecureRandom.uuid.gsub(/\-/,'')
  end
end
