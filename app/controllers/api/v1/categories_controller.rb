class Api::V1::CategoriesController < ApplicationController

  load_and_authorize_resource

  def index
    render json: @categories
  end

  def create
    if already_exists
      render json: {:message => 'Category with that username already exists'}, status: 409
    elsif @category.save
      render json: @category, status: :ok
    else
      render json: @category.errors, status: :unprocessable_entity
    end
  end

  def update
    if @category.update_attributes(category_params)
      render json: @category, status: :ok
    else
      render json: @category.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @category.destroy
      render nothing: true
    else
      render json: @category.errors, status: :unprocessable_entity
    end
  end

  private

  def category_params
    params.require(:category).permit(:category_name)
  end

  def already_exists
    Category.find_by(:category_name => params[:category][:category_name])
  end
end
