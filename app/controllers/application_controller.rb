class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  helper_method :current_user
   before_action :restrict_access

  rescue_from CanCan::AccessDenied do |exception|
    render json: {:message => exception.message}, status: 403
  end

  def current_user
    authenticate_token
  end

  def current_ability
    @current_ability ||= Ability.new(current_user, params)
  end

  def restrict_access
    authenticate_token || render_unauthorized
  end

  def authenticate_token
    authenticate_with_http_token do |token, options|
      User.find_by(auth_token:token)
    end
  end

  def render_unauthorized
    self.headers['WWW-Authenticate'] = 'Token realm = "Application"'
    render json: {:message => 'You are Not Logged In',}, status: :unauthorized
  end

  def default_serializer_options
    {root: false}
  end
end
