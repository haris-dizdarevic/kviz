class AssignedQuiz < ActiveRecord::Base
  belongs_to :user
  belongs_to :quiz
  attr_accessor :username
  validates :user_id,
            :presence => true
  validates :quiz_id,
            :presence => true
  validates :active,
            :inclusion => {:in => [true,false]}
end
