class SavedQuiz < ActiveRecord::Base
  belongs_to :result
  belongs_to :answer
  validates :answer_id,
            :presence => true
  validates :result_id,
            :presence => true
end
