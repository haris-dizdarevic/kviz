class Category < ActiveRecord::Base
  has_many :quizzes, :dependent => :destroy

  validates :category_name,
            :presence => true,
            :length => {:within => 5..25}

end
