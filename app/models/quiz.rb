class Quiz < ActiveRecord::Base
  has_many :questions, :dependent => :destroy
  has_many :results, :dependent => :destroy
  has_many :assigned_quizzes, :dependent => :destroy
  belongs_to :category
  belongs_to :user

  validates :user_id,
            :presence => true
  validates :category_id,
            :presence => true
  validates :quiz_type,
            :presence => true,
            :length => {:minimum => 4}
end
