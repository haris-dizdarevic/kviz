class Answer < ActiveRecord::Base
  has_many :saved_quizzes, :dependent => :destroy
  belongs_to :question

  validates :answer_text,
            :presence => true,
            :length => {:minimum => 5}
  validates :question_id,
            :presence => true
end
