class Result < ActiveRecord::Base
  has_many :saved_quizzes, :dependent => :destroy
  belongs_to :user
  belongs_to :quiz

  validates :quiz_id,
            :presence => true
  validates :user_id,
            :presence => true

end
