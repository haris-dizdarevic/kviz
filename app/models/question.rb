class Question < ActiveRecord::Base
  has_many :answers, :dependent => :destroy
  belongs_to :quiz
  validates :question_text,
            :presence => true,
            :length => {:minimum => 10}
  validates :quiz_id,
            :presence => true

end
