class User < ActiveRecord::Base
  has_secure_password

  has_many :quizzes, :dependent => :destroy
  has_many :results, :dependent => :destroy
  has_many :assigned_quizzes

  EMAIL_REGEX = /\A[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}\Z/i
  LETTERS_REGEX = /[A-Za-z]/

  validates :username,
            :presence => true,
            :length => {:within => 5..20},
            :format => LETTERS_REGEX
  validates :first_name,
            :presence => true,
            :length => {:within => 2..20},
            :format => LETTERS_REGEX
  validates :last_name,
            :presence => true,
            :length => {:within => 5..25},
            :format => LETTERS_REGEX
  validates :email,
            :presence => true,
            :uniqueness => true,
            :format => EMAIL_REGEX
  validates :password_digest,
            :presence => true
  validates :role,
            :presence => true

end
