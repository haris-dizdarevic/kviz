class Ability
  include CanCan::Ability

  def initialize(user, params)
    user ||= User.new
    alias_action :create, :read, :update, :destroy, :to => :crud
    if user.role == 'admin'
      puts 'admin'
      can :crud, :all
      cannot :destroy, User, ['role = ?'] do |some_user|
        some_user.role = 'admin'
      end
    elsif user.role == 'user'
      puts 'user'
      can :read, User, :id => user.id
      can [:read, :update, ], AssignedQuiz, :user_id => user.id
      can :read, [Quiz, Category, Answer]
      can :read, SavedQuiz, ['result_id = ?',params[:result_id]] do |saved_answer|
        saved_answer.result_id == params[:result_id]
      end
      can :read, Question, ['quiz_id = ?', params[:quiz_id]] do |question|
        question.quiz_id = params[:quiz_id]
      end
      can :read, Result, :user_id => user.id
      can :create, [Result, SavedQuiz, SavedAnswer]
    end
  end
end
